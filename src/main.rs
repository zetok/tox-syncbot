/*
    Copyright © 2015-2016 Zetok Zalbavar <zetok@openmailbox.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



/*
    Binding to toxcore
*/
extern crate rstox;
use rstox::core::*;


/*
    For various stuff
*/
extern crate rand;
use rand::ThreadRng;
use rand::Rng;

/*
    For syncing with IRC
*/
extern crate irc;
use irc::client::prelude::*;
use std::sync::Arc;
use std::thread::spawn;
use std::sync::mpsc::channel;


/*
    For loading and writing Tox data
*/
extern crate chrono;
use chrono::UTC;

mod for_files;
mod for_strings;


// TODO: just use channels from the config
static CHANNEL: &'static str = "##channelname";


/*
    Bot's own stuff
*/
// TODO: when other functions will be moved from main.rs, things should be
//       added here
mod bootstrap;



pub struct Bot {
    /**
        Tox struct.
    */
    tox: Tox,

    /**
        Bot name.
    */
    // TODO: load from config file
    name: String,

    /**
        Cached RNG, apparently it helps with RNG's performance when it's used
        a lot.
    */
    random: ThreadRng,

    /**
        Time since last save.
    */
    last_save: i64,

    /**
        Time since last message was sent to IRC. Needed, for ratelimiting.
    */
    last_message: i64,
}


impl Bot {
    /**
        Create new `Bot` struct.

        Takes data for toxcore's instance to load.
    */
    fn new(data: Option<Vec<u8>>) -> Self {
        Bot {
            tox: Tox::new(ToxOptions::new(), data.as_ref()
                                            .map(|x| &**x)).unwrap(),

            name: "Relay".to_string(),
            last_save: UTC::now().timestamp(),
            random: rand::thread_rng(),
            last_message: UTC::now().timestamp(),
        }
    }


    /// set a new name, or default one if nothing is provided
    fn set_name(&mut self, new: Option<String>) {
        if let Some(name) = new {
            drop(self.tox.set_name(&name));
        } else {
            drop(self.tox.set_name(&self.name));
        }
    }
}





#[derive(Debug,Clone)]
struct ToxMessage {
    nick:    String,
    message: String,
}

/**
    Should be sent to IRC channel(s?).
*/
impl ToxMessage {
    fn new(n: String, m: String) -> Self {
        ToxMessage { nick: n, message: m}
    }

    // aside from giving a nice format, make sure to "escape" properly chars
    // that by default can't be sent to IRC
    fn format_message(self) -> String {
        let msg = format!("{}: {}", self.nick, self.message);
        for_strings::sanitize_for_irc(&msg)
    }
}



#[derive(Debug,Clone)]
struct IrcMessage {
    nick:    String,
    message: String,
    action:  bool,
}

/**
    `IrcMessage` is supposed to be sent to Tox groupchats.
*/
impl IrcMessage {
    fn new(name: String, msg: String, action: bool) -> Self {
        IrcMessage { nick: name, message: msg, action: action }
    }

    fn to_irc_message(msg: &Message) -> Option<Self> {
        if let Some(ref prefix) = msg.prefix {
            if let Some(ref message) = msg.suffix {
                if &msg.command == "PRIVMSG" {
                    // get nick
                    let nick = prefix[..prefix.find('!').unwrap_or(2)].to_string();
                    // now that there's usable nick, get the message
                    // TODO: also detect if that an action message
                    return Some(IrcMessage {
                        nick: nick,
                        message: (&message).to_string(),
                        action: false
                    })
                }
            }
        }
        None
    }

    fn send_message(self, bot: &mut Bot) {
        // additionally sending name in the message is only needed since
        // groupchats are broken & don't handle well fast nick changes
        let msg = format!("[{}] {}", &self.nick, &self.message);
        bot.set_name(Some(self.nick));
        let groups = bot.tox.get_chatlist();
        for group in groups {
                drop(bot.tox.group_message_send(group, &msg));
                // Since toxcore should tick at least 20 times per second
                // to stay online, and there is no knowledge about amount
                // of groupchats that bot is in, there is no reason to
                // believe that "sending" message to all the groupchats that
                // bot is in would take less than 1/20 of a second. Thus after
                // sending message to each group ensure that toxcore "ticks".
                // This slightly increases CPU usage, the more groupchats,
                // the bigger increase will be.
                bot.tox.tick()
        }
        bot.set_name(None);
    }
}



/**
    Get some random data as UTF-8 string of `len` length and a source of
    randomness.
*/
fn rand_string(len: usize, rng: &mut ThreadRng) -> String {
    let mut vec: Vec<u8> = Vec::with_capacity(len);

    for _ in 0..len {
        vec.push(rng.gen::<u8>());
    }

    unsafe { String::from_utf8_unchecked(vec) }
}


fn on_irc_message(msg: Message) -> Option<(String, String)> {
    if let Some(message) = IrcMessage::to_irc_message(&msg) {
        println!("{}: Added IRC message: '{:?}'", UTC::now(), &message);
        Some((message.nick, message.message))
    }
    else {
        println!("{}: {:?}", UTC::now(), &msg);
        None
    }
}


/*
    Function to deal with incoming friend requests, all are accepted.
*/
fn on_friend_request(bot: &mut Bot, fpk: PublicKey, msg: String) {
    drop(bot.tox.add_friend_norequest(&fpk));
    println!("\n{}: Friend {} with friend message {:?} was added.",
            UTC::now(), fpk, msg);
}

/*
    Function to deal with incoming invites to groupchats
*/
fn on_group_invite(bot: &mut Bot, fid: i32, kind: GroupchatType, data: Vec<u8>) {
    /*
        Since rstox currently supports only text groupchats, handle only them,
        and drop other invites.
    */
    match kind {
        GroupchatType::Text => {
            drop(bot.tox.join_groupchat(fid, &data));
            println!("{}: Accepted invite to text groupchat by {}.",
                    UTC::now(), fid);
        },
        GroupchatType::Av => {
            println!("{}: Declined invite to audio groupchat by {}.",
                    UTC::now(), fid);
        },
    }
}


/*
    Function to deal with friend messages.

    Every message should be replied with a random message.

    Message can trigger constant stream of random messages, turn it off,
    or send an ID to friend.

*/
fn on_friend_message(bot: &mut Bot, fnum: u32, msg: String) {
    let reply_msg: String =
        rand_string((1372.0 * bot.random.gen::<f64>()) as usize, &mut bot.random);

    match &*msg {
        ".id" | "id" | "ID" => {
            let message = format!("My ID: {}", bot.tox.get_address());
            drop(bot.tox.send_friend_message(fnum, MessageType::Normal, &message));
        },

        _ => drop(bot.tox.send_friend_message(fnum, MessageType::Normal,
                &reply_msg)),
    }
}

fn on_group_message(bot: &mut Bot, gnum: i32, pnum: i32, msg: String, messages: &mut Vec<ToxMessage>) {
    if let Some(nick) = bot.tox.group_peername(gnum, pnum) {
        if let Some(pkey) = bot.tox.group_peer_pubkey(gnum, pnum) {
            if pkey != bot.tox.get_public_key() {
                let new_msg = ToxMessage::new(nick, msg);
                // Insert new message on position `0`, so that it would be
                // dispatched as the last one from queue.
                messages.insert(0, new_msg);
            }
        }
    }
}




fn main() {
    /*
        Try to load data file, if not possible, print an error and generate
        new Tox instance.
    */
    let data = for_files::load_save("bot.tox")
                .map_err(|e| println!("\n{}: Error loading save: {}\n",
                                      UTC::now(), e))
                .ok();

    /*
        Bot stuff
    */
    let mut bot = Bot::new(data);


    bot.set_name(None);
    drop(bot.tox.set_status_message("I'm relay. Whatcha want?"));


    // IRC part of the bot
    let (tx_irc, rx_irc) = channel();
    let (tx_tox, rx_tox) = channel();
    std::thread::spawn(move || {
        let server = Arc::new(IrcServer::new("config.json").unwrap());
        server.identify().unwrap();


        // to join the channel
        let serv_join = server.clone();
        std::thread::spawn(move || {
            std::thread::sleep_ms(10000);
            let _ = serv_join.send_join(CHANNEL);
        });

        let mut times_ran_irc: usize = 0;

        let server1 = server.clone();
        std::thread::spawn(move || {
            loop {
                if let Ok(msg) = rx_tox.recv() {
                    let msg: String = msg;
                    let _ = server1.send_privmsg(CHANNEL, &msg);
                }
            }
        });

        let tx_irc = tx_irc.clone();
        let server = server.clone();
        std::thread::spawn(move || {
            for m in server.iter() {
                match m {
                    Ok(m) => {
                        //let mut messages = Vec::new();
                        if let Some(msg) = on_irc_message(m) {
                            tx_irc.send(msg).unwrap();
                        }
                    },
                    e => {
                        println!("{}: {:?}", UTC::now(), e);
                    },
                }
                times_ran_irc += 1;
                println!("\n{}: IRC iter ran {} times.", UTC::now(), &times_ran_irc);
            }
        });
    });


    let mut tox_messages: Vec<ToxMessage> = Vec::new();
    let mut irc_messages: Vec<IrcMessage> = Vec::new();


    /*
        Boostrapping process
        During bootstrapping one should query random bootstrap nodes from a
        supplied list; in case where there is no list, rely back on hardcoded
        bootstrap nodes.
        // TODO: actually make it possible to use supplied list; location of a
        //       list should be determined by value supplied in config file;
        //       in case of absence of config file, working dir should be
        //       tried for presence of file named `bootstrap.txt`, only if it
        //       is missing fall back on hardcoded nodes
    */
    bootstrap::bootstrap_hardcoded(&mut bot.tox);

    println!("\nMy ID: {}", bot.tox.get_address());
    println!("My name: {:?}", bot.tox.get_name());



    loop {
        for ev in bot.tox.iter() {
            match ev {
                FriendRequest(fpk, msg) => {
                    on_friend_request(&mut bot, fpk, msg);
                },

                FriendMessage(fnum, _msgkind, msg) => {
                    on_friend_message(&mut bot, fnum, msg);
                },

                GroupInvite(gnum, gtype, data) => {
                    on_group_invite(&mut bot, gnum, gtype, data);
                },

                GroupMessage(gnum, pnum, message) => {
                    on_group_message(&mut bot, gnum, pnum, message, &mut tox_messages);
                },

                _ => println!("{}: Event: {:?}", UTC::now(), ev),
            }
        }

        // Receive messages from IRC.
        if let Ok(msg) = rx_irc.try_recv() {
            let (nick, message) = msg;
            let msg = IrcMessage::new(nick, message, false);
                // Insert new message at position `0`, so that dispatching
                // queue would be done ~chronologically.
                irc_messages.insert(0, msg);
        }

        // send Tox messages to IRC
        // ratelimit to message per second
        let now_time = UTC::now().timestamp();
        if bot.last_message + 1 < now_time {
            if let Some(msg) = tox_messages.pop() {
                let m: String = msg.format_message();
                tx_tox.send(m).unwrap();
            }
            bot.last_message = now_time;
        }

        // send IRC messages to groupchats
        if let Some(msg) = irc_messages.pop() {
            msg.send_message(&mut bot);
        }


        /*
            Write save data every 64s.

            After a write, be it successful or not, set clock again to tick,
            for the next time when it'll need to be saved.
            TODO: save data every $relevant_event, rather than on timer.
        */
        let cur_time = UTC::now().timestamp();
        if bot.last_save + 64 < cur_time {
            match for_files::write_save("bot.tox", bot.tox.save()) {
                Ok(_) => println!("{}: File saved.", UTC::now()),
                Err(e) => println!("\n{}: Failed to save file: {}",
                                UTC::now(), e),
            }
            bot.last_save = cur_time;
        }

    // needed to run
    bot.tox.wait();
    }
}

