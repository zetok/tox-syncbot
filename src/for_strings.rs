/*
    Copyright © 2016 Zetok Zalbavar <zetok@openmailbox.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! Stuff for string manipulation.


/** Make sure that string from Tox groupchat can be sent to IRC.

In order to get that:

 * escape `\n` to literal ` |\n| `
 * escape `\0` to literal `\0`
*/
pub fn sanitize_for_irc(s: &str) -> String {
    s.replace("\n", " |\\n| ")
     .replace("\0", "\\0")
}
