This is a syncbot for Tox. It's the best `;)`

More features may come along the way `:)`

Any contributions are welcome.

Tox ID of a running bot:
`1BF52E0BE84EAE5A1D32EA4C9E42907555F275D595E1936A4F2858D968914909FE12B2A8D6DC`


# Features

* spam for 1-on-1 chats
* saving & loading Tox data file


# Installation
Installation is fairly simple. This bot will work only on Linux.

Newest [**toxcore**](https://github.com/irungentoo/toxcore) is required. For
instructions on how to get it, please refer to its
[INSTALL.md](https://github.com/irungentoo/toxcore/blob/master/INSTALL.md).

1. Install [Rust](http://www.rust-lang.org/)
2. Make with `cargo build`
3. Run with `./target/debug/./relay

# Usage

Currently supported by bot commands are:

## Friend commands

| Command | What it does |
|---------|--------------|
| id | Messages its ID |


# License

Licensed under GPLv3+, for details see [COPYING](/COPYING).
